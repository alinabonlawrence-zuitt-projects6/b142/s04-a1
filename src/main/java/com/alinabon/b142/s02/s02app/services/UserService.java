package com.alinabon.b142.s02.s02app.services;

import com.alinabon.b142.s02.s02app.models.User;

public interface UserService {
    Iterable<User> getUser();
    void createUser(User newUser);
    void updateUser(Long id, User existingUser);
    void deleteUser(Long id);
}
